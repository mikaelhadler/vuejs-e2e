// <reference types="Cypress" />
//let Login = require('../../page-objects/login.po')
const Login = require('../../page-objects/login.po')
const CreateAccount = require('../../page-objects/create-account.po')

describe('Automation Practice', () => {
  let email = 'user' + Math.random() + '@mail.com'
  let pass = 'password'

  beforeEach(function () {
    cy.visit('http://automationpractice.com/');
  })

  it('Create Account', () => {
    cy.get(Login.botaoSignin).click()
    cy.get(CreateAccount.email).type(email)
    cy.get(CreateAccount.submitCreate).click()

    cy.get(CreateAccount.gender).check('1')
    cy.get(CreateAccount.customerFirstName).type("First Name")
    cy.get(CreateAccount.customerLastName).type("Last Name")

    cy.get(CreateAccount.password).type(pass)
    cy.get(CreateAccount.days).select('3')
    cy.get(CreateAccount.months).select('December')
    cy.get(CreateAccount.years).select('1985')

    cy.get(CreateAccount.firstName).clear().type("First Name")
    cy.get(CreateAccount.lastName).clear().type("Last Name")
    cy.get(CreateAccount.company).type("Company")
    cy.get(CreateAccount.address1).type("Street Address One")
    cy.get(CreateAccount.address2).type("Address Two")

    cy.get(CreateAccount.city).type("Austin")

    cy.get(CreateAccount.country).select('United States')
    cy.get(CreateAccount.states).select("Texas")

    cy.get(CreateAccount.postCode).type(55555)

    cy.get(CreateAccount.additionalInformation).type("Additional Information")

    cy.get(CreateAccount.phone).type('55577777')
    cy.get(CreateAccount.phoneMobile).type('999977778')

    cy.get(CreateAccount.addressAlias).clear().type("Main Address")

    cy.get(CreateAccount.submitAccount).click()

    cy.get('a.logout').should('be.visible')
    cy.screenshot()
    cy.get('a.logout').click()
  })

  it('Sign In validation', () => {
    cy.get(Login.botaoSignin).click()
    cy.get(Login.email).type(email)
    cy.get(Login.password).type(pass)
    cy.get(Login.submitSignin).click()

    cy.get('a.logout').should('be.visible')
    cy.screenshot()
    cy.get('a.logout').click()
  })

  it('Cart', () => {
    cy.login(email,pass)
    cy.get('a[title="View my shopping cart"]').click()
    cy.get('h1#cart_title').should('be.visible')
    cy.screenshot()    
  })

  it('Add item to Cart', () => {
    cy.login(email,pass)
    cy.get('a[title="Women"]').click()    
    cy.get('a.button[data-id-product="1"]').trigger('mouseover').click()
    cy.get('a[title="Proceed to checkout"]').click()
    cy.get('.ajax_cart_quantity').contains('1')
    cy.screenshot()
  })
})