# marcelloMello-testAPI

API Test using Cypress framework 

Pre-requirements:
* Nodejs 8

## How to run tests:

Install dependencies with `npm install` or `npm ci`

See scripts in `package.json` to run the tests.

* `npm run cy:open` - open Cypress GUI mode
* `npm run cy:run` - runs Cypress tests against it in GUI mode
* `npm test` - runs Cypress tests against it in headless mode


To run WEB tests is necessary to remove .skip command at automation-practice.js file under specs folder. 